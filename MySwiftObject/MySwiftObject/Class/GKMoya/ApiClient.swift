//
//  ApiClient.swift
//  MySwiftObject
//
//  Created by 王炜圣 on 2021/8/12.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit
//曼奇尼 曼奇尼 曼因斯坦 
class ApiClient: HandyJSON {
    //MARK:换皮肤就需要更改这个颜色,默认经典蓝色
    var theme     :GKTheme?     = nil
    //MARK:男生还是女生
    var sex       :GKUserState = .boy
    //MARK:网格还是列表
    var gird      :GKLookState = .list
    //MARK:
    var rankDatas : [GKRankModel]? = nil
    //夜间模式
    var night     :Bool        = false
    required public init() {}
}
extension ApiClient{
    static var client: ApiClient = {
        let data = UserDefaults.standard.object(forKey:moya.defaultClient)
        let json = JSON(data as Any)
        guard let info = ApiClient.deserialize(from: json.rawString()) else { return ApiClient() }
        return info
    }()
    ///MARK:更换皮肤颜色
    static func setTheme(_ theme :GKTheme){
        guard let fileName = theme.fileName else { return }
        if moya.client.theme?.fileName != fileName {
            moya.client.theme = getTheme(fileName)
            saveClient(moya.client)
        }
    }
    static func setSex(_ sex :GKUserState){
        if moya.client.sex != sex{
            moya.client.sex = sex
            ApiClient.saveClient(moya.client)
        }
    }
    static func setRankData(_ rankData :[GKRankModel]){
        moya.client.rankDatas = rankData
        ApiClient.saveClient(moya.client)
    }
    static func setGrid(_ grid :GKLookState){
        if moya.client.gird != grid{
            moya.client.gird = grid
            ApiClient.saveClient(moya.client)
        }
    }
    //MARK:读取默认配置文件
    static var defaultTheme :GKTheme{
        return getTheme("defaults")
    }
    //MARK:读取默认配置文件
    static var nightTheme :GKTheme{
        return getTheme("night")
    }
    //夜间模式
    static func setNight(_ night :Bool){
        if moya.client.night != night{
            moya.client.night = night
            ApiClient.saveClient(moya.client)
        }
    }
    //MARK:根据文件名读取配置文件
    static func getTheme(_ plistName :String) ->GKTheme{
        guard let url = Bundle.main.url(forResource: plistName, withExtension: "plist") else { return GKTheme() }
        guard let data = try? Data(contentsOf: url) else { return GKTheme() }
        guard let dic = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) else { return GKTheme() }
        let json = JSON(dic)
        guard let theme = GKTheme.deserialize(from: json.rawString()) else { return GKTheme() }
        theme.fileName = plistName
        return theme
    }
    //MARK:保存信息
    fileprivate static func saveClient(_ client :ApiClient){
        let defaults = UserDefaults.standard
        guard let dic = client.toJSON() else { return }
        defaults.set(dic, forKey: moya.defaultClient)
        defaults.synchronize()
    }
    //MARK:删除信息
    fileprivate static func deleteClient(){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: moya.defaultClient)
        defaults.synchronize()
        moya.client = ApiClient()
    }
}
