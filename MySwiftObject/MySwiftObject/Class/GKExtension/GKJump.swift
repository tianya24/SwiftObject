//
//  GKJump.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//
import UIKit
import MGJRouter_Swift
import SwiftUI

//后期改为路由管理
class GKJump: NSObject {
    static func jumpToDetail(bookId:String? = ""){
        MGJRouter.open(AppDetail, ["bookId": bookId as Any]) { result in
            
        }
    }
    static func jumpToMore(homeInfo:GKHomeInfo){
        let vc  = GKHomeMoreTabController(info:homeInfo);
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
    }
    static func jumpToClassifyTail(group:String,name:String){
        let vc = GKClassifyTailController(group: group, name: name)
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
    }
    static func jumpToBookCase(){
        let root  =  UIViewController.rootTopPresentedController()
        root.tabBarController?.selectedIndex = 0
    }
    static func jumpToNovel(bookId :String? = "" ,chapter : NSInteger = 0){
        MGJRouter.open(AppRead, ["bookId":bookId as Any,"chapter":chapter]) { result in
            
        }
    }
    static func jumpToBrowse(){
        MGJRouter.open(AppBrowse)
    }
    static func jumpToSetController(){
        MGJRouter.open(AppSet) { result in
            debugPrint(result as Any)
        }
    }
    static func jumpToSearch(){
        MGJRouter.open(AppSearch)
    }
    static func jumpToDownView(){
        MGJRouter.open(AppDown)
    }
    static func jumpToThemeController(){
        MGJRouter.open(AppTheme)
    }
    static func jumpToDownContent(content :GKMyDownContent){
        let vc = UIHostingController(rootView: GKMyDownContentView.init(model: content))
        vc.showNavTitle(title: "阅读")
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    static func jumpToSearchResult(keyWord :String){
        let vc = GKSearchResultController(keyWord: keyWord)
        vc.hidesBottomBarWhenPushed = true;
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true);
    }
    static func jumpToGridController(){
        let vc = GKGridController()
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    static func jumpToStackController(){
        let vc = GKStackViewController(nibName: "GKStackViewController_iPad", bundle: nil)
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    static func jumpToLogoController(){
        let vc = GKMyLogoController()
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    static func jumpToMan(){
        let vc = GKManTabController()
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    static func jumpToMulu(bookId :String){
        let vc = GKChapterController(bookId: bookId)
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    static func jumpToUser(){
        let vc = GKUserController()
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    static func jumpToWebView(){
        let vc = UIHostingController(rootView: GKMyAgreeView())
        vc.showNavTitle(title: "隐私政策")
        vc.hidesBottomBarWhenPushed = true
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }

}
