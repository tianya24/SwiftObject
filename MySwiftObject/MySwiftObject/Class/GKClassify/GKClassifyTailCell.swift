//
//  GKClassifyTailCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKClassifyTailCell: BaseTableCell {

    @IBOutlet weak var openBtn: UIButton!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var subTitleLab: UILabel!
    
    @IBOutlet weak var countLab: UILabel!
    @IBOutlet weak var stateBtn: UIButton!
    @IBOutlet weak var focusBtn: UIButton!
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var nickNameBtn: UIButton!
    public var openClick : ((_ model :GKHistory) ->Void)? = nil
    var browse : GKHistory?{
        didSet{
            guard let item = browse else { return }
            let time :String = Date().timeStampToMinute(timeStamp: item.insertTime)
            self.model = item.book
            self.subTitleLab.text = "\n阅读时间:\(time)\n"
            self.openBtn.isHidden = false
        }
    }
    var model : GKBook?{
        didSet{
            guard let item = model else { return }

            self.imageV.setGkImageWithURL(url: item.cover  ?? "")
            self.titleLab.text = item.title ?? ""
            self.subTitleLab.attributedText  = attTitle(content: item.shortIntro ?? "")
            self.countLab.text = item.latelyFollower.getCount
            self.stateBtn.setTitle(item.majorCate ?? "", for: .normal)
            self.stateBtn.isHidden = item.majorCate?.count == 0 ? true : false
            self.focusBtn.setTitle("关注:"+String(item.retentionRatio)+("%"), for: .normal)
            self.nickNameBtn .setTitle(item.author, for: .normal)
            self.nickNameBtn.isHidden = item.author.count > 0 ? false : true
            self.openBtn.isHidden = true
        }
    }
     func attTitle(content :String) -> NSAttributedString {
        let paragraphStyle  = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .left
        paragraphStyle.allowsDefaultTighteningForTruncation = true
         let att = NSAttributedString.init(string:content.count > 0 ? content : "这家伙很懒,暂无简介!", attributes:[NSAttributedString.Key.paragraphStyle : paragraphStyle,NSAttributedString.Key.foregroundColor : UIColor.appx999999()])
        return att
    }
    @IBAction func openAction(_ sender: UIButton) {
        guard let openClick = self.openClick else { return }
        openClick(self.browse!)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.stateBtn.layer.masksToBounds = true
        self.stateBtn.layer.cornerRadius = 7.5
        self.focusBtn.layer.masksToBounds = true
        self.focusBtn.layer.cornerRadius = 10
        self.focusBtn.setTitleColor(moya.appColor, for: .normal)
        self.nickNameBtn.layer.masksToBounds = true
        self.nickNameBtn.layer.cornerRadius = AppRadius
        self.nickNameBtn.backgroundColor = UIColor.appxf8f8f8()
        self.nickNameBtn.setTitleColor(UIColor.appx666666(), for: .normal)
        
        self.openBtn.layer.masksToBounds = true
        self.openBtn.layer.cornerRadius = 13
        self.openBtn.layer.borderWidth = 0.6
        self.openBtn.layer.borderColor = UIColor.appxdddddd().cgColor
        
        self.lineView.backgroundColor = UIColor.appxdddddd()
        self.titleLab.textColor = UIColor.appx333333()
        self.subTitleLab.textColor = UIColor.appx666666()
        
        self.imageV.layer.masksToBounds = true
        self.imageV.layer.cornerRadius = AppRadius
    }
    
}
