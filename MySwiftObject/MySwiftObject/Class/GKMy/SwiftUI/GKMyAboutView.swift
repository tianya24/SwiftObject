//
//  GKMyAboutView.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import SwiftUI
import WebKit

struct GKMyAboutView: View {
    var body: some View {
        VStack(alignment: .center, spacing: 50) {
            Spacer()
            VStack(alignment: .center, spacing: 20) {
                Image("icon_start")
                Text(moya.appName).font(Font.system(size: 16)).foregroundColor(Color(UIColor.appx000000()))
            }
            Spacer()
            HStack {
                Button {
                    GKJump.jumpToGridController()
                } label: {
                    Text("用户协议").font(Font.system(size: 14)).foregroundColor(Color(UIColor.appx666666()))
                }.offset(x: 50, y: 0)
                Spacer()
                Button {
                    GKJump.jumpToWebView()
                    //GKJump.jumpToStackController()
                } label: {
                    Text("隐私政策").font(Font.system(size: 14)).foregroundColor(Color(UIColor.appx666666()))
                }.offset(x: -50, y: 0)
            }.offset(x: 0, y: -30)
        }
    }
}
struct GKMyAgreeView :View{
    var body: some View {
        VStack {
            BaseWebView(urlString: "http://www.baidu.com")
        }
    }
}
struct BaseWebView :UIViewRepresentable{
    @State var urlString :String
    func makeUIView(context: Context) -> some UIView {
        return WKWebView()
    }
    func updateUIView(_ uiView: UIViewType, context: Context) {
        guard let url = URL(string: urlString) else { return }
        let request = URLRequest(url:url)
        if let webView = uiView as? WKWebView{
            webView.load(request)
        }
    }
}

