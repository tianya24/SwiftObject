//
//  GKMyModel.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

struct GKMyModel {
    var title:String    = "";
    var icon:String     = "";
    var subTitle:String = "";
    var switchOn:Bool?  = nil
}
struct BMyTheme {
    var title :String? = "经典蓝"
    var color :String? = "3991F0"
    var select:Bool?   = false
}

struct GKActionSheet{
    var title :GKHomeMenu
    var icon  :String
    var enable:Bool = true
}
