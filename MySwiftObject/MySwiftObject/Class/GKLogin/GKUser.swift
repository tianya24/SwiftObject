//
//  GKUser.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/9.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

public class GKUser: HandyJSON {
    var account :String? = ""//账号
    var token   :String? = ""//用户token
    var avatar  :String? = ""//用户avatar
    required public init() {}
}

