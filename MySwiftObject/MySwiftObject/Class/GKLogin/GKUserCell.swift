//
//  GKUserCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/10.
//  Copyright © 2022 wangws1990. All rights reserved.
//  
import UIKit

class GKUserCell: BaseTableCell {
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var imageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageV.layer.masksToBounds = true
        self.imageV.layer.cornerRadius = 30
        self.lineView.backgroundColor = UIColor.appxdddddd()
        self.titleLab.textColor = UIColor.appx000000()
    }
    var model :GKMyModel?{
        didSet{
            guard let item = model else { return }
            self.imageV.setGkImageWithURL(url: item.icon)
            self.titleLab.text = item.title
        }
    }
}

class GKNickNameCell : BaseTableCell{
    lazy var titleLab: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        label.textColor = UIColor.appx333333()
        
        return label
    }()
    lazy var subTitleLab: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.textColor = UIColor.appx666666()
        return label
    }()
    lazy var lineView : UIView = {
        let lineView = UIView()
        lineView.backgroundColor = UIColor.appxdddddd()
        return lineView
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        self.contentView.addSubview(self.subTitleLab);
        self.contentView.addSubview(self.titleLab);
        self.contentView.addSubview(self.lineView)
        self.titleLab.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(15)
            make.top.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().offset(-20)
        }
        self.subTitleLab.snp.makeConstraints { make in
            make.centerY.equalTo(self.titleLab)
            make.right.equalToSuperview().offset(-15)
            make.left.equalTo(self.titleLab.snp.right).offset(10)
        }
        self.titleLab.setContentHuggingPriority(.defaultLow, for: .horizontal)
        self.subTitleLab.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        self.lineView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview().offset(0)
            make.height.equalTo(0.8)
        }
    }
    var model :GKMyModel?{
        didSet{
            guard let item = model else { return }
            self.titleLab.text = item.title
            self.subTitleLab.text = item.subTitle
        }
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
