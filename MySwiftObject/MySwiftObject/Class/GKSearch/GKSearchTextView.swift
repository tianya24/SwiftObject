//
//  GKSearchTextView.swift
//  ATSeek
//
//  Created by wangws1990 on 2018/6/22.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit
import RxSwift
@objc protocol GKSearchDelegate :NSObjectProtocol{
     @objc optional func searchTopView(topView:GKSearchTextView,keyword:String)
     @objc optional func searchTopView(topView:GKSearchTextView,goback :Bool)
}
class GKSearchTextView: BaseView,UITextFieldDelegate {
    
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var textField: UITextField!
    weak var delegate :GKSearchDelegate? = nil
    public lazy var disposeBag = DisposeBag()
    public var keyword : String?{
        didSet{
            guard let keyword = keyword else { return }
            self.textField.text = keyword
            self.textAction(sender: self.textField)
        }
    }
    private var canTap : Bool?{
        didSet{
            guard let canTap = canTap else { return }
            self.searchBtn.isSelected = canTap
            self.searchBtn.isUserInteractionEnabled = canTap
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textField.delegate = self
        self.mainView.layer.masksToBounds = true
        self.mainView.layer.cornerRadius = AppRadius * 2
        self.mainView.backgroundColor = UIColor.appxf4f4f4()
        self.textField.tintColor = UIColor.appx333333()
        self.textField.textColor = UIColor.appx333333()
        self.textField.backgroundColor = UIColor.appxf4f4f4()
        
        self.searchBtn.layer.masksToBounds = true
        self.searchBtn.layer.cornerRadius = AppRadius
        self.top.constant = STATUS_BAR_HIGHT
        
        
        self.searchBtn.setBackgroundImage(UIImage.imageWithColor(color:moya.appColor), for: .selected)
        self.searchBtn.setBackgroundImage(UIImage.imageWithColor(color:moya.appColor), for: UIControl.State(rawValue: UIControl.State.selected.rawValue | UIControl.State.highlighted.rawValue))
        
        
        self.searchBtn.setBackgroundImage(UIImage.imageWithColor(color:UIColor.appx999999()), for: .normal)
        self.searchBtn.setBackgroundImage(UIImage.imageWithColor(color:UIColor.appx999999()), for: UIControl.State(rawValue: UIControl.State.normal.rawValue | UIControl.State.highlighted.rawValue))
        
        self.searchBtn.isSelected = false
//        self.textField.addTarget(self, action: #selector(textAction(sender:)), for: .editingChanged)
        
//        self.searchBtn.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
//        self.backBtn.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        
        _ = self.backBtn.rx.tap.bind {[weak self] () in
            self?.backAction()
        }.disposed(by: disposeBag)
        _ = self.searchBtn.rx.tap.bind(onNext: { [weak self]() in
            self?.searchAction()
        }).disposed(by: disposeBag)
        _ = self.textField.rx.text.bind(onNext: { [weak self]objc in
            if let text = objc{
                self?.canTap = text.count > 0
            }else{
                self?.canTap = false
            }
        }).disposed(by: disposeBag)
    }
    @objc func textAction(sender:UITextField){
        self.canTap = sender.text!.count > 0
    }
    @objc func searchAction(){
        guard let delegate = self.delegate else { return }
        delegate.searchTopView?(topView: self, keyword: self.textField.text!)
    }
    @objc func backAction(){
        guard let delegate = self.delegate else { return }
        delegate.searchTopView?(topView: self, goback: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.count == 0 {
            textField.resignFirstResponder();
            return false;
        }
        searchAction()
        return true;
    }

}

