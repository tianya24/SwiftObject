//
//  GKChapterController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/3/31.
//  Copyright © 2020 wangws1990. All rights reserved.
//
import UIKit
import SwiftyJSON
//
class GKChapterController: BaseTableViewController {
    convenience init(bookId :String) {
        self.init()
        self.bookId = bookId
    }
    private var bookId    : String? = nil
    private lazy var chapterDatas : [JSON] = {
        return []
    }()
    private lazy var slider: VSSlider = {
        let slider =  VSSlider()
        slider.isHidden = true
        slider.vertical = true
        slider.ascending = true
        slider.minimumValue = 0
        slider.maximumValue = 1.0
        slider.thumbImage = UIImage(named: "ic_book_set_up_down_1")
        slider.thumbTintColor = UIColor.clear
        slider.minimumTrackTintColor = UIColor.clear
        slider.maximumTrackTintColor = UIColor.clear
        slider.maximumTrackImage = UIImage.imageWithColor(color: UIColor.clear)
        slider.minimumTrackImage = UIImage.imageWithColor(color: UIColor.clear)
        return slider
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "章节列表")
        self.tableView.scrollsToTop = false
        self.setupRefresh(scrollView: self.tableView, options: .defaults);
        self.view.addSubview(self.slider)
        self.slider.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(-TAB_BAR_ADDING)
            make.width.equalTo(20)
        }
        self.tableView.snp.remakeConstraints { make in
            make.left.top.bottom.equalToSuperview()
            make.right.equalTo(self.slider.snp.left)
        }
        self.slider.addTarget(self, action: #selector(touchUpAction(sender:)), for: .touchDown);
        self.slider.addTarget(self, action: #selector(outsideAction(sender:)), for: UIControl.Event(rawValue: UIControl.Event.touchUpInside.rawValue|UIControl.Event.touchUpOutside.rawValue|UIControl.Event.touchCancel.rawValue));
        self.slider.addTarget(self, action: #selector(changedAction(sender:)), for: .valueChanged);
    }
    override func refreshData(page: Int) {
        guard let bookId = self.bookId else { return }
        GKHistoryData.getCacheChapters(bookId: bookId) { model in
            if let info = model,let json = info.chapters{
                self.slider.isHidden = false
                self.chapterDatas = json
            }
            self.tableView.reloadData()
            self.endRefresh(more: false)
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chapterDatas.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = GKNovelDirectoryCell.cellForTableView(tableView: tableView, indexPath: indexPath);
        cell.selectionStyle = .none;
        let model  = self.chapterDatas[indexPath.row];
        cell.json = model
        return cell;
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let bookId = self.bookId else { return }
        GKJump.jumpToNovel(bookId:bookId,chapter: indexPath.row)
    }
//    override var refreshVertica: CGFloat{
//        return -100
//    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.changeValue{
            return
        }
        var p : CGFloat = 0;
        let height1 : CGFloat = scrollView.contentSize.height
        let height2 : CGFloat = scrollView.frame.size.height
        p = scrollView.contentOffset.y/(height1 - height2 + (40 - 40 * height2/height1));
        p = p.isNaN ? 0 : p;
        self.slider.value = Float(p)
    }
    public var changeValue :Bool = false
    @objc  func touchUpAction(sender:UISlider){
        self.changeValue = true
    }
    @objc  func outsideAction(sender:UISlider){
        self.changeValue = false
    }
    @objc  func changedAction(sender:UISlider){
        let myHeight = SCREEN_HEIGHT - NAVI_BAR_HIGHT
        let value = sender.value
        var hegiht = self.tableView.contentSize.height
        hegiht = hegiht * CGFloat(value)
        hegiht = hegiht > myHeight ? hegiht - myHeight : hegiht
        self.tableView.setContentOffset(CGPoint(x: 0, y: hegiht), animated: false)
    }
}
